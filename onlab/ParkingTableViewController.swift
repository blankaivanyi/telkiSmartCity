//
//  ParkingViewController.swift
//  onlab
//
//  Created by Iványi Blanka on 2018. 04. 01..
//  Copyright © 2018. Iványi Blanka. All rights reserved.
//

import Foundation
import UIKit

var list = ["Parkolások ideje", "Parkolóhelyek telítettsége", "Historikus kimutatás"]
var descriptions = ["Parkolások idejének százalékos kimutatása","Parkolóhelyek telítettsége","Historikus kimutatás a parkolóhelyek átlagos használatáról"]
var myIndex = 0

class ParkingTableViewController : UITableViewController {
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (list.count)
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
        cell.textLabel?.text = list[indexPath.row]
        
        return (cell)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        myIndex = indexPath.row
        if (myIndex == 0) {
            performSegue(withIdentifier: "parkingTimeSegue", sender: self)
        }
        if (myIndex == 1) {
            performSegue(withIdentifier: "parkingSpotsSegue", sender: self)
        }
        if (myIndex == 2) {
            performSegue(withIdentifier: "historySegue", sender: self)
        }
    }
}
