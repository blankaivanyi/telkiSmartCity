//
//  File.swift
//  onlab
//
//  Created by Iványi Blanka on 2018. 04. 01..
//  Copyright © 2018. Iványi Blanka. All rights reserved.
//

import Foundation
import Charts
import Alamofire

class ParkingHistoryViewController : UIViewController {
    
    @IBOutlet weak var chartView: BarChartView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //let data = [BarChartDataEntry(x: 1, y: 2),
        //            BarChartDataEntry(x: 2, y: 4),
        //            BarChartDataEntry(x: 3, y: 3),
        //            ]
        //dataSet.setColors(NSUIColor.red, NSUIColor.blue, NSUIColor.green)
     

            Alamofire.request("https://telki-smartcity.herokuapp.com/api/parking/average").responseJSON { response in
                print("Response: \(String(describing: response.response))") // http url response
                print("Result: \(response.result)")                         // response serialization result
                
                if let json = response.result.value as! [[String:AnyObject]]? {
                    var data = [] as [BarChartDataEntry]
                    for obj in json {
                        data.append(BarChartDataEntry(x: obj["id"] as! Double, y: obj["value"] as! Double))
                    }
                    let dataSet = BarChartDataSet(values: data, label: " ")
                    dataSet.setColors(NSUIColor.red, NSUIColor.green, NSUIColor.blue, NSUIColor.magenta, NSUIColor.orange)
                    let barChartData = BarChartData()
                    barChartData.addDataSet(dataSet)
                    self.chartView.data = barChartData
                    self.chartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: json.map {$0["day"] as! String})

                }
                
            }
            
        }
        
    }
