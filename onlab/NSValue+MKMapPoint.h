//
//  NSValue+MKMapPoint.h
//  onlab
//
//  Created by Iványi Blanka on 2018. 03. 25..
//  Copyright © 2018. Iványi Blanka. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

#ifndef NSValue_MKMapPoint_h
#define NSValue_MKMapPoint_h

@interface NSValue (MKMapPoint)

+ (NSValue * _Nonnull )valueWithMKMapPoint:(MKMapPoint)mapPoint;

@end

#endif /* NSValue_MKMapPoint_h */
