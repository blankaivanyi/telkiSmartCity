//
//  TelkiAnnotation.swift
//  onlab
//
//  Created by Iványi Blanka on 2018. 03. 26..
//  Copyright © 2018. Iványi Blanka. All rights reserved.
//

import MapKit

class TelkiAnnotation: NSObject, MKAnnotation {
    var title: String?
    var subtitle: String?
    var coordinate: CLLocationCoordinate2D
    
    init(title:String, subtitle:String, coordinate:CLLocationCoordinate2D) {
        self.title = title
        self.subtitle = subtitle
        self.coordinate = coordinate
    }
}
