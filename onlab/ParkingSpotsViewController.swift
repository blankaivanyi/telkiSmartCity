//
//  ParkingSpotsViewController.swift
//  onlab
//
//  Created by Iványi Blanka on 2018. 04. 01..
//  Copyright © 2018. Iványi Blanka. All rights reserved.
//

import Foundation
import Alamofire

class ParkingSpotsViewController : UIViewController {
    
    @IBOutlet weak var numberOfEmptySpotsLabel: UILabel!
    @IBOutlet weak var numberOfOccupiedSpotsLabel: UILabel!
    @IBOutlet weak var isDisabledOccupiedLabel: UILabel!
    let image = UIImage(named: "telki.jpg")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        Alamofire.request("https://telki-smartcity.herokuapp.com/api/parking/status").responseJSON { response in
            print("Response: \(String(describing: response.response))") // http url response
            print("Result: \(response.result)")                         // response serialization result
            
            if let json = response.result.value as! [String:AnyObject]? {
                self.numberOfEmptySpotsLabel.text = String(json["free"] as! Int) + " db"
                self.numberOfOccupiedSpotsLabel.text = String(json["occupied"] as! Int) + " db"
                if(json["disabledFree"] as! Bool) {
                    self.isDisabledOccupiedLabel.text = "szabad"
                }
                else {
                    self.isDisabledOccupiedLabel.text = "foglalt"
                }
            }
        }
    }
}
