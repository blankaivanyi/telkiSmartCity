//
//  ParkingTimesViewController.swift
//  onlab
//
//  Created by Iványi Blanka on 2018. 04. 01..
//  Copyright © 2018. Iványi Blanka. All rights reserved.
//

import Foundation
import UIKit
import Charts
import Alamofire

class ParkingTimesViewController : UIViewController {
    @IBOutlet weak var pieView: PieChartView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Alamofire.request("https://telki-smartcity.herokuapp.com/api/parking/times").responseJSON { response in
            print("Response: \(String(describing: response.response))") // http url response
            print("Result: \(response.result)")                         // response serialization result
            
            if let json = response.result.value as! [[String:AnyObject]]? {
                var data = [] as [PieChartDataEntry]
                for obj in json {
                    data.append(PieChartDataEntry(value: obj["value"] as! Double, label: obj["bucket"] as! String))
                }
                let dataSet = PieChartDataSet(values: data, label: "parkolási idők hossza")
                dataSet.setColors(NSUIColor.red, NSUIColor.green, NSUIColor.blue, NSUIColor.magenta, NSUIColor.orange)
                let pieChartData = PieChartData()
                pieChartData.addDataSet(dataSet)
                self.pieView.data = pieChartData
            }
            
        }
    
    }
}
