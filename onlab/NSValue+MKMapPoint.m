//
//  NSValue+MKMapPoint.m
//  onlab
//
//  Created by Iványi Blanka on 2018. 03. 25..
//  Copyright © 2018. Iványi Blanka. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@implementation NSValue (MKMapPoint)

+ (NSValue *)valueWithMKMapPoint:(MKMapPoint)mapPoint {
    return [NSValue value:&mapPoint withObjCType:@encode(MKMapPoint)];
}

@end
