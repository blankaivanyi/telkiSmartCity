//
//  ViewController.swift
//  onlab
//
//  Created by Iványi Blanka on 2018. 03. 25..
//  Copyright © 2018. Iványi Blanka. All rights reserved.
//

import UIKit
import MapKit
import DTMHeatmap
import Alamofire

class TelkiMapViewController: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var heatmapButton: UIButton!
    var heatmapOverlay:MKOverlay?
    @IBAction func heatmapButton(_ sender: UIButton) {
        if(heatmapButton.currentTitle == "Hőtérkép") {
            sender.setTitle("Térkép", for: UIControlState.normal)
            self.map.add(heatmapOverlay!)
        }
        else {
            sender.setTitle("Hőtérkép", for: UIControlState.normal)
            self.map.remove(heatmapOverlay!)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let distanceSpan:CLLocationDegrees = 2000
        let telkiLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(47.549839, 18.82463)
        map.setRegion(MKCoordinateRegionMakeWithDistance(telkiLocation, distanceSpan, distanceSpan), animated: true)
        
        Alamofire.request("https://telki-smartcity.herokuapp.com/api/temp/map").responseJSON { response in
            print("Response: \(String(describing: response.response))") // http url response
            print("Result: \(response.result)")                         // response serialization result
            
            if let json = response.result.value as! [[String:AnyObject]]? {
                var pins = [] as [TelkiAnnotation]
                for obj in json {
                    pins.append(TelkiAnnotation(title: obj["name"] as! String, subtitle: String(obj["temp"] as! Double) + "°C", coordinate: CLLocationCoordinate2DMake(obj["long"] as! Double, obj["lat"] as! Double)))
                }
                self.map.addAnnotations(pins)

            }
            
        }

        let heatmap = DTMHeatmap()
        heatmapOverlay = heatmap
        let data = NSMutableDictionary()
        data.setObject(3.0, forKey: NSValue(mkMapPoint: MKMapPointForCoordinate(telkiLocation)))
        data.setObject(2.0, forKey: NSValue(mkMapPoint: MKMapPointForCoordinate(CLLocationCoordinate2DMake(47.559839, 18.82463))))
        for i in 0...1000 {
            let random1 = (drand48() - 0.5) / 30
            let random2 = (drand48() - 0.5) / 30
            data.setObject(1.0, forKey: NSValue(mkMapPoint: MKMapPointForCoordinate(CLLocationCoordinate2DMake(47.549839 + random1, 18.82463 + random2))))
        }
        heatmap.setData(data as! [AnyHashable : Any])
        //map.add(heatmap)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        return DTMHeatmapRenderer.init(overlay: overlay)
    }

}

